var express = require('express');
var router = express.Router();
var User = require('../models/user.js')
var Judge = require('../models/judge')

/* GET users listing. */
/*router.get('/', function(req, res, next) {
	var user = new User()
  	res.send('respond with a resource');
})*/
router.route('/testSetup')
	.get(function(req, res) {
		var user = new User()
		
		user.name = "John Doe"
		user.username = "jdoe"
		user.password = "password"
		
		user.save(function(err) {
			if (err){
				if (err.code == 11000) return res.json({ success: false, message: 'A user with that username already exists'} )
				else return res.send(err)
			}
			return res.json({ success: true, message: "User created"})
		})
	})

router.route('/')
	.get(function(req, res) {
		
		var query = User.find()
		
		query.select('name username')
		
		query.exec(function(err, users) {
			if (err) return res.send(err)
			
			return res.json({ success: true, users: users})
		})
		
		/*User.find(function(err, users) {
			if (err) return res.send(err)
			
			return res.json({ success: true, users: users})
		})*/
	})

	.post(function(req, res) {
		var user = new User(req.body)
		
		user.save(function(err) {
			if (err) return res.send(err)
			
			return res.json({ success: true, message: "Added user"})
		})
	})
	
	.delete(function(req, res) { 
		User.remove(function(err) {
			if (err) return res.send(err)
			
			res.json({ success: true, message: 'All users deleted' })
		})
	})
	
router.route('/:user_id')
	.get(function(req, res) {
		User.findById(req.params.user_id, function(err, user) {
			if(err) return res.send(err)
			
			if(!user) return res.status(404).json({ success: false, message: "User not found"})
			
			return res.json({success: true, user: user})
		})
	})
	
	.put(function(req, res) {
		User.findById(req.params.user_id, function(err, user) {
			if (err) return res.send(err)
			
			if(!user) return res.status(404).json({ success: false, message: "User not found" })
			
			for (var prop in req.body) {
				user[prop] = req.body[prop]
			}
			
			user.save(function(err) {
				if (err) return res.send(err)
				
				res.json({ success: true, message: "User update!" })
			})
		})
	})
	
	.delete(function(req, res) {
		Judge.remove({ user : req.params.user_id}, function(err, judge) {
			if(err) return res.send(err)
		})
		
		User.findByIdAndRemove(req.params.user_id, function(err, user) {
			if(err) return res.send(err)
			
			if(!user) return res.status(404).json({ success: false, message: "User not found"})
			
			return res.json({ success: true, message: "User Deleted!" })
		})
	})
  

module.exports = router;
