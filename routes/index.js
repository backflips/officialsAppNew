var express = require('express');
var router = express.Router();
var jwt = require('express-jwt')
var config = require('../config/config.js')

var auth = jwt({ secret: config.secret,
                    getToken: function(req) {
                        var token = req.body.token || req.query.token || req.headers['x-access-token'];
                        if(token)
                            return token;
                        else
                            return null;   
                    }
            })

router.use('/login', require('./login'))
router.use('/user', auth, require('./users'))
router.use('/judge', auth, require('./judge'))
router.use('/competition', auth, require('./competition'))

router.use('/test', require('./test'))

/* GET home page. */
router.get('/', function(req, res, next) {
  res.json({ message  :   'Welcome to the API'  })
});

router.use(function(err, req,res,next) {
	if(err.name === 'UnauthorizedError') {
		res.status(401).json({ success: false, message: "Please sign in to access the api"})
	}
})

router.use(function(req, res, next) {
	res.status(404).json({ success: false, message: "The requested data was not found" })
})

module.exports = router;
