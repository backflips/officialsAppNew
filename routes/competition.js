var express = require('express');
var router = express.Router();
var Competition = require('../models/competition')

router.route('/')
	.get(function(req, res) {
		var query = Competition.find()
		
		query.exec(function(err, competitions) {
			if (err) return res.send(err)
			
			return res.json({ success: true, competitions: competitions})
		})
	})
	
	.post(function(req, res) {
		var judge = new Judge(req.body)
		
		judge.save(function(err) {
			if(err) return res.send(err)
			
			return res.json({ success: true, message: "Added user"})
		})
		
	})
	
	
module.exports = router