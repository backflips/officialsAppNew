var express = require('express');
var router = express.Router();
var User = require('../models/user.js')

/* GET users listing. */
/*router.get('/', function(req, res, next) {
	var user = new User()
  	res.send('respond with a resource');
})*/
router.route('/')
	.get(function(req, res) {
		var user = new User()
		
		user.name = "John Doe"
		user.username = "jdoe"
		user.password = "password"
		
		user.save(function(err, user) {
			if (err){
				if (err.code == 11000) return res.json({ success: false, message: 'A user with that username already exists'} )
				else return res.send(err)
			}
			
			var token = user.generateAdminToken()
			return res.json({ success: true, message: "User created", user: user, token: token})
		})
	})
	
router.route('/admin')

	.get(function(req, res) {
		User.findOne({ username: 'jdoe'}, function(err, user) {
			if (err) return res.send(err)
			console.log(user)
			var token = user.generateJWT()
			
			return res.json({token : token})
		})
	})
	
module.exports = router;