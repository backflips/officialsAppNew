var express = require('express');
var router = express.Router();
var User = require('../models/user.js')
var passport = require('passport')
var config = require('../config/config')


router.route('/')

	.post(function(req, res, next) {
		passport.authenticate('local', function(err, user, info) {
			if(err) return next(err)
			
			if(!user) {
				return res.status(401).json(info)
			}
			else {
				var token = user.generateJWT()
			
				return res.json({ success: true, user: token})
			}
			
		})(req, res, next)
	})

/*router.get('/', function(req, res, next) {
	return res.json({success: true, message: "Change this"})
})*/

router.route('/register')

	.post(function(req, res) {
		var user = new User(req.body)
		
		user.save(function(err) {
			if (err) return res.send(err)
			
			return res.json({ success: true, message: "Added user"})
		})
	})

module.exports = router