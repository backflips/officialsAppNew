var express = require('express');
var router = express.Router();
var Judge = require('../models/judge')

router.route('/')
	.get(function(req, res) {
		var query = Judge.find()

		query.populate('user', 'name')
		
		query.exec(function(err, judges) {
			if (err) return res.send(err)
			
			return res.json({ success: true, judges: judges})
		})
	})
	
	.post(function(req, res) {
		var judge = new Judge(req.body)
		
		judge.save(function(err) {
			if(err) return res.send(err)
			
			return res.json({ success: true, message: "Added user"})
		})
		
	})
	
router.route('/:judge_id')

	.get(function(req, res) {
		var query = Judge.findById(req.params.judge_id)
		
		query.populate('user', 'name')
		query.exec(function(err, judge) {
			if(err) return res.send(err)
			
			if(!judge) return res.status(404).json({ success: false, message: "Could not find that judge" })
			
			return res.json({ sucess: true, judge: judge })
		})
	})
	
	.put(function(req, res) {
		Judge.findById(req.params.judge_id, function(err, judge) {
			if (err) return res.send(err)
			
			if(!judge) return res.status(404).json({ success: false, message: "Judge not found" })
			
			for (var prop in req.body) {
				judge[prop] = req.body[prop]
			}
			
			judge.save(function(err) {
				if (err) return res.send(err)
				
				res.json({ success: true, message: "Judge updated!" })
			})
		})
	})

	.delete(function(req, res) {
		Judge.findByIdAndRemove(req.params.judge_id, function(err, judge) {
			if(err) return res.send(err)
			
			if(!judge) return res.status(404).json({ success: false, message: "Judge not found"})
			
			return res.json({ success: true, message: "Judge Deleted!" })
		})
	})

module.exports = router