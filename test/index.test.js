var supertest = require('supertest')
var expect = require('chai').expect
var should = require('chai').should()
var api = supertest('http://localhost:3000')


describe('Index Page', function() {
	describe('GET /', function() {
		it('should return 404', function(done) {
			api.get('/')
				.expect(404)
			done()
		})
	})
	
	describe('POST /', function() {
		it('should return 404', function(done) {
			api.post('/')
				.expect(404)
			done()
		})
	})
	
	describe('PUT /', function() {
		it('should return 404', function(done) {
			api.put('/')
				.expect(404)
			done()
		})
	})
	
	describe('DELETE /', function() {
		it('should return 404', function(done) {
			api.delete('/')
				.expect(404)
			done()
		})
	})
})

describe('API page', function() {
	describe('GET /api', function() {
		it('should send a welcome message', function(done) {
			api.get('/api')
				.expect(200)
				.expect('Content-Type', /json/)
				.end(function(err, res) {
					expect(res.body).to.have.property("message")
					expect(res.body.message).to.not.equal(null)
					done()
				})
		})
	})
	
	describe('POST /api', function() {
		it('should send 404 json', function(done) {
			api.post('/api')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(function(err, res) {
					expect(res.body).to.have.property("success")
					expect(res.body.success).to.equal(false)
					expect(res.body).to.have.property("message")
					expect(res.body.message).to.equal("The requested data was not found")
					done()
				})
		})
	})
	
	describe('PUT /api', function() {
		it('should send 404 json', function(done) {
			api.put('/api')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(function(err, res) {
					expect(res.body).to.have.property("success")
					expect(res.body.success).to.equal(false)
					expect(res.body).to.have.property("message")
					expect(res.body.message).to.equal("The requested data was not found")
					done()
				})
		})
	})
	
	describe('DELETE /api', function() {
		it('should send 404 json', function(done) {
			api.delete('/api')
				.expect(404)
				.expect('Content-Type', /json/)
				.end(function(err, res) {
					expect(res.body).to.have.property("success")
					expect(res.body.success).to.equal(false)
					expect(res.body).to.have.property("message")
					expect(res.body.message).to.equal("The requested data was not found")
					done()
				})
		})
	})
	
	/*it('should return a 200 response', function(done) {
		api.get('/api')
			.expect(200, done)
	})
	it('should return a message', function(done) {
		api.get('/api')
			.expect(200)
			.end(function(err, res) {
				expect(res.body).to.have.property('message')
				expect(res.body.message).to.not.equal(null)
				expect(res.body.message).to.equal("this is a test")
				res.body.message.should.be.a('string')
				done()
			})
	})
	it('404 on post', function(done) {
		api.put('/api')
			.send({
				name: "CASDF"
			})
			.expect(404, done)
	})*/
	
})