var supertest = require('supertest')
var expect = require('chai').expect
var should = require('chai').should()
var api = supertest('http://localhost:3000')

describe('User Endpoint', function() {
	describe('unauthenticated', function() {
		
		it('GET should reuturn 401', function(done) {
			api.get('/api/user')
				.expect(401, done)
		})
		
		it('POST should reuturn 401', function(done) {
			api.post('/api/user')
				.expect(401, done)
		})
		
		it('PUT should reuturn 401', function(done) {
			api.put('/api/user')
				.expect(401, done)
		})
		
		it('DELETE should reuturn 401', function(done) {
			api.delete('/api/user')
				.expect(401, done)
		})	
	})
	/*describe('authenticated', function() {
		var token = null
		before(function(done) {
			api.get('')
				.expect('Content-Type', /json/)
				.expect(200)
				.end(function(err, res) {
					expect(res.body).to.have.property("token")
					expect(res.body.token).to.not.equal(null)
					token = res.body.token
					done()
				})
				
		})
		
		it('GET should reuturn 200', function(done) {
			console.log(token)
			api.get('/api/user')
				.set('x-access-token', token)
				.expect(200, done)
		})
		
	})*/
})