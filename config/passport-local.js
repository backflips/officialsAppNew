var passport = require('passport')
var LocalStrategy = require('passport-local').Strategy
var mongoose = require('mongoose')
var User = mongoose.model('User')

passport.use(new LocalStrategy(
	function(username, password, done) {
		User.findOne({
			username	:	username
		}).select('name username password').exec(function(err, user) {
			if (err) return done(err)
			
			if(!user) {
				return done(null, false, {success: false, message: 'That username or password is not recognized' })
			}
			
			if(!user.comparePassword(password)) {
				return done(null, false, {success: false, message: 'That username or password is not recognized' })
			}
			
			return done(null, user)
		})
	}
))