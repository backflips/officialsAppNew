var databaseName = 'testThing'
var secret = "Thisistotallyjustatest"

var connected = function(err) {
	if(err) console.log('Connection Error: ', err)
	else {
		console.log('Connection Successful.  Connected to: ' + databaseName)
	}
}

module.exports = {
	'databaseName'	:	databaseName,
	'database'		:	'mongodb://localhost:27017/' + databaseName,
	'connected'		:	connected,
	'secret'		:	secret,
}