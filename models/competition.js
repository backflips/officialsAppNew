var mongoose = require('mongoose')
var Schema = mongoose.Schema

var CompetitionSchema = new Schema({
	name		: 	String,
	location	:	String,
	host		:	String,
	startDate	:	Date,
	endDate		:	Date
})

module.exports = mongoose.model('Competition', CompetitionSchema)