var mongoose = require('mongoose')
var Schema = mongoose.Schema

require('./user')


var JudgeSchema = new Schema({
	discipline	: 	{ type: String},
	user		:	{ type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	club		:	String,
	level		:	String
})

module.exports = mongoose.model('Judge', JudgeSchema)