var mongoose = require('mongoose')
var Schema = mongoose.Schema
var bcrypt = require('bcrypt-nodejs')
var jwt = require('jsonwebtoken')
var config = require('../config/config')


var UserSchema = new Schema({
	name	: 	{ type: String},
	username:	{ type: String, required: true, unique: true},
	password:	{ type: String, required: true, select: false},
	phone	: 	String,
	email	: 	String,
	address	: 	String
})

UserSchema.pre('save', function(next) {
	var user = this
		
	if(!user.isModified('password')) return next()
	
	bcrypt.hash(user.password, null, null, function(err, hash) {
		if (err) return next(err)
		
		user.password = hash
		next()
	})
})

UserSchema.methods.comparePassword = function(password) {
	var user = this
	return bcrypt.compareSync(password, user.password)
}

UserSchema.methods.generateJWT = function() {
	var today = new Date()
	var exp = new Date()
	
	exp.setDate(today.getDate())
	
	return jwt.sign({
		_id			:	this._id,
		name		:	this.name,
		username	:	this.username,
		exp			:	parseInt(exp.getTime()/1000 + 3600)
	}, config.secret)
}

UserSchema.methods.generateAdminToken = function() {
	return jwt.sign({
		_id		:		this._id,
		name		:	this.name,
		username	:	this.username
	}, config.secret)
}

module.exports = mongoose.model('User', UserSchema)